#!/usr/bin/python
# -*- coding: UTF-8 -*-

import csv
from grafo import Grafo
from collections import deque, Counter
from heapq import heappush, heappop
import random

# USAR HEAPQ como heap
# USAR DEQUE como cola

class Sistema(object):
  """docstring for sistema"""
  def __init__(self):
    self.grafo = Grafo()
    self.peliculas = 0 
    self.camino_minimo = []
    self.padres_camino = []
    self.BFS = []
    self.padres_BFS = []

    # Guardar referencia al BFS y al camino minimo

def BFS_inicial(sistema,actor):
  if(sistema.BFS != [] && sistema.padres_BFS != []): return

  padres,orden = BFS(sistema.grafo,actor)
  sistema.BFS = orden
  sistema.padres_BFS = padres

def camino_minimo_inicial(sistema.actor):
  if(sistema.camino_minimo != [] && sistema.padres_camino != []): return

  padres,costos = camino_minimo(sistema.grafo,actor)
  sistema.camino_minimo = costos
  sistema.padres_camino = padres

def grafo_crear(nombre_archivo):
  """
  Crea un grafo de conexiones de actores a partir de un archivo de datos.

  PRE: Recibe el nombre de un archivo separado por comas que contenga de lineas:
      actor,pelicula,pelicula,pelicula
      que equivalen a: vertice,arista,arista,arista
  POST: Devuelve un grafo creado a partir de estos datos.
  """

  sistema = Sistema()
  grafo = sistema.grafo
  peliculas = {}

  with open(nombre_archivo) as archivo:
    lineas = csv.reader(archivo)
    
    for linea in lineas:
      actor = None
      for index, dato in enumerate(linea):
        if index == 0:
          actor = dato
          grafo.agregar_vertice(actor)
          continue

        peliculas[dato] = peliculas.get(dato, [])
        peliculas[dato].append(actor)

  actores = 0
  for pelicula in peliculas:
    pivote = peliculas[pelicula][0]
    actores += 1
    for actor in peliculas[pelicula]:
      if actor == pivote: continue
      grafo.agregar_arista(pivote,actor,pelicula)
      peliculas[pelicula].pop(1)
  return sistema

def camino(sistema, origen, llegada):
  """
  Devuelve el camino entre un actor de origen y uno de llegada.

  PRE: Recibe el grafo, un actor de origen y un actor de llegada.
  POST: Devuelve una lista ordenada de cadenas (películas) para llegar desde
      el origen hasta el final.
  """

  camino_minimo_inicial(sistema,llegada)

  padres = sistema.padres_camino
  costos = sistema.camino_minimo

  camino = []
  actual = origen
  
  while (actual in visitados and actual in padres):
    
    if (padres[actual] == None): 
      break
    
    camino.append((actual,padres[actual],''.join(grafo.obtener_arista(actual,padres[actual]))))
    actual = padres[actual] 
  
  return camino

def actores_a_distancia(sistema, origen, n):
  """
  Devuelve los actores a distancia n del recibido.

  PRE: Recibe el grafo, el actor de origen y el número deseado.
  POST: Devuelve la lista de cadenas (actores) a n pasos del recibido.
  """
  BFS_inicial(sistema, origen)

  orden =sistema.BFS 
  padres = sistema.padres_BFS

  heap = []
  for vertice in costos:
    if costos[vertice] == n:
      heappush(heap,vertice)
  actores = []
  while (heap != []):
    actores.append(heappop(heap))
  return actores

def popularidad(sistema, actor):
  """
  Calcula la popularidad del actor recibido.

  PRE: Recibe el grafo y un actor de origen
  POST: Devuelve un entero que simboliza la popularidad: todos los adyacentes
      de los adyacentes del actor, multiplicado por su cantidad de peliculas
  """

  BFS_inicial(sistema, actor)

  orden = sistema.BFS 
  padres = sistema.padres_BFS
  
  suma = 0
  for vertice in ORDEN:
    if orden[vertice] == 2:
      suma += 1
  multiplicacion = 0
  visitados = {}
  for adyacente in grafo.obtener_adyacentes(actor):
    for arista in grafo.obtener_arista(adyacente,actor):
      if arista not in visitados:
        visitados[arista] = True
        multiplicacion += 1
  return suma * multiplicacion



def similares(sistema,origen, n):
  """
  Calcula los n actores más similares al actor de origen y los devuelve en una
  lista ordenada de mayor similitud a menor.

  PRE: Recibe el grafo, el actor de origen, y el n deseado
  POST: Devuelve una lista de los n actores no adyacentes más similares al
      pedido. La lista no debe contener al actor de origen.
  """

  counter = Counter()
  counter[origen] += 0
  
  repeticiones = grafo.cantidad_de_vertices()
  actual = origen
  
  while repeticiones > 0:
    adyacentes = grafo.obtener_adyacentes(actual)
    actual = random.choice(adyacentes)
    
    counter[actual] += 1 
    repeticiones -= 1

  return counter.most_common(n) 

#----------------------------------------------------------------------------------------------------------

def camino_minimo(grafo,origen):
  visitados = {}
  padres = {}
  costos = {}
  heap = [] #Size infinito
  heappush(heap,(0,origen))
  visitados[origen] = True
  padres[origen] = None
  costos[origen] = 0
  while (heap != []):
    v = heappop(heap)
    for w in grafo.obtener_adyacentes(v[1]):
      nuevo_costo = v[0] + 1
      if (w not in costos or costos[w] > nuevo_costo):
        visitados[w] = True
        padres[w] = v[1]
        costos[w] = nuevo_costo
        heappush(heap,(costos[w],w))
  return padres,costos


def BFS(grafo,origen):
  visitados = {}
  padre = {}
  orden = {}
  cola = deque()
  cola.append(origen)
  visitados[origen] = True
  orden[origen] = 0
  padre[origen] = None
  while (cola != deque([])):
    v = cola.popleft()
    for w in grafo.obtener_adyacentes(v):
      if w not in visitados:
        visitados[w] = True
        padre[w] = v
        orden[w] = orden[v] + 1
        cola.append(w)
  return padre,orden

# def dfs_visitar (grafo, v, visitados, padre, orden):
  
#   visitados[v] = True
#   for w in grafo.obtener_adyacentes(v):
#     if w not in visitados:
#       padre[w] = v
#       orden[w] = orden[v] + 1
#       dfs_visitar(grafo, w, visitados, padre, orden)

# def DFS(grafo):
  
#   visitados = {}
#   padre = {}
#   orden = {}
#   for v in grafo.obtener_vertices():
#     if v not in visitados:
#       padre[v] = None
#       orden[v] = 0
#       dfs_visitar(grafo,v,visitados,padre,orden)

#   return padre,orden

# ---------------------------------------------------------------------------------------------------------

def camino_hasta_KB(sistema, actor):

  camino1 = camino(sistema, actor,"Bacon Kevin")  
  solucion = ""
  if (camino1 == []):
    solucion += "No hay conexion con Kevin Bacon"
  for conexion in camino1:
    solucion += str(conexion[0]) + " actuo con " + str(conexion[1]) + " en " + str(conexion[2]) + '\n'
  print (solucion) 

def bacon_number(sistema, actor):
  
  camino1 = camino(sistema, actor,"Bacon Kevin")    
  number = -1
  if (camino1 != []):
    number += len(camino1) + 1
  solucion = str(actor) + " tiene un Kevin Bancon number igual a " + str(number) + '\n'
  print (solucion) 

def bacon_number_infinito(sistema):

  BFS_inicial(sistema,"Bacon Kevin")

  visitados = sistema.BFS

  sumatoria = 0
  for vertice in grafo.obtener_vertices():
    if vertice not in visitados:
      sumatoria += 1

  solucion = "Los actores con un Bacon Number infinito son " + str(sumatoria) + '\n'
  print (solucion)

def bacon_number_mayor_a_6(sistema):
  
  BFS_inicial(sistema.grafo,"Bacon Kevin")

  orden = sistema.BFS

  counter = Counter()
  for vertices in orden:
    if orden[vertices] >= 6:
      counter[orden[vertices]] += 1
  
  elementos = counter.most_common(len(counter))
  elementos.sort()

  solucion = "Los actores con un KBN mayor a 6 son:\n"
  for distancia in elementos:
    solucion +=  "Con KBN igual a " + str(distancia[0]) + ": " + str(distancia[1]) + " actores \n"
  print(solucion)

