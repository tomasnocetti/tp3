#!/usr/bin/python
# -*- coding: UTF-8 -*-

import csv
from grafo import Grafo
from collections import deque, Counter
from heapq import heappush, heappop
import random

# USAR HEAPQ como heap
# USAR DEQUE como cola

class Grados(object):

  def __init__(self, nombreArchivo):
    """
    Inicialisa al objeto con un archivo de datos valido.

    PRE: Recibe el nombre de un archivo separado por comas que contenga de lineas:
      actor,pelicula,pelicula,pelicula
      que equivalen a: vertice,arista,arista,arista
    POST: Devuelve un  creado a partir de estos datos.
    """

    peliculas = {}
    self.grafo = Grafo()
    self.actores = 0

    with open(nombreArchivo) as archivo:

      lineas = csv.reader(archivo)
      for linea in lineas:
        actor = None
        for index, dato in enumerate(linea):
          if index == 0:
            actor = dato
            self.grafo.agregar_vertice(actor)
            self.actores += 1
            continue

          peliculas[dato] = peliculas.get(dato, [])
          peliculas[dato].append(actor)

      lineas = None

    self.peliculas = len(peliculas)
    for pelicula in peliculas:

      self.grafo.agregar_arista_multiple(peliculas[pelicula], pelicula, 0)
      actores = None

    self.ultimo_camino = None

  def camino_minimo(self, origen):

    if not self.grafo.pertenece(origen): return [],[]

    if (self.ultimo_camino and self.ultimo_camino[0] == origen):
      return self.ultimo_camino[1], self.ultimo_camino[2]

    grafo = self.grafo

    visitados = {}
    padre = {}
    orden = {}
    cola = deque()
    cola.append(origen)
    visitados[origen] = True
    orden[origen] = 0
    padre[origen] = None
    while (cola != deque([])):
      v = cola.popleft()
      for w in grafo.obtener_adyacentes(v):
        if w in visitados: continue
        visitados[w] = True
        padre[w] = v
        orden[w] = orden[v] + 1
        cola.append(w)

    self.ultimo_camino = (origen, padre, orden)
    return padre, orden

  def camino(self, origen, llegada):
    """
    Devuelve el camino entre un actor de origen y uno de llegada.

    PRE: Recibe el grafo, un actor de origen y un actor de llegada.
    POST: Devuelve una lista ordenada de cadenas (películas) para llegar desde
        el origen hasta el final.
    """

    grafo = self.grafo

    if not grafo.pertenece(origen) or not grafo.pertenece(llegada): return None

    padres, costos = self.camino_minimo(llegada)

    camino = []
    actual = origen

    while (actual in padres):
      if (padres[actual] == None):
        break

      item = (actual, padres[actual], ''.join(grafo.obtener_arista(actual, padres[actual])))
      camino.append(item)
      actual = padres[actual]

    return camino

  def actores_a_distancia(self, origen, n):
    """
    Devuelve los actores a distancia n del recibido.

    PRE: Recibe el grafo, el actor de origen y el número deseado.
    POST: Devuelve la lista de cadenas (actores) a n pasos del recibido.
    """

    if n < 0 or not self.grafo.pertenece(origen): return None

    padres, costos = self.camino_minimo(origen)

    heap = []
    for vertice in costos:
      if costos[vertice] == n:
        heappush(heap, vertice)
    actores = []

    while (heap != []):
      actores.append(heappop(heap))
    return actores

  def popularidad(self, actor):
    """
    Calcula la popularidad del actor recibido.

    PRE: Recibe el grafo y un actor de origen
    POST: Devuelve un entero que simboliza la popularidad: todos los adyacentes
        de los adyacentes del actor, multiplicado por su cantidad de peliculas
    """
    if not self.grafo.pertenece(actor): return None

    padres, costos = self.camino_minimo(actor)

    grafo = self.grafo

    distancia_2 = self.actores_a_distancia(actor, 2)

    return len(distancia_2) * len(grafo.obtener_aristas(actor))

  def similares(self, origen, n):
    """
    Calcula los n actores más similares al actor de origen y los devuelve en una
    lista ordenada de mayor similitud a menor.

    PRE: Recibe el grafo, el actor de origen, y el n deseado
    POST: Devuelve una lista de los n actores no adyacentes más similares al
        pedido. La lista no debe contener al actor de origen.
    """
    if not self.grafo.pertenece(origen) or n < 0: return None

    grafo = self.grafo

    counter = Counter()
    counter[origen] += 0

    repeticiones = grafo.cantidad_de_vertices()
    actual = origen


    while repeticiones > 0:
      adyacentes = list(grafo.obtener_adyacentes(actual))
      actual = random.choice(adyacentes)

      counter[actual] += 1
      repeticiones -= 1

    mas_comunes = list(counter.most_common(n + 1))
    similares = []

    for actor, apariciones in mas_comunes:
      if origen != actor: similares.append(actor)

    if len(similares) == n + 1: similares.pop()

    return similares

  def cantidad_peliculas(self):
    return self.peliculas

  def cantidad_actores(self):
    return self.actores

  def obtener_actores(self):
    return self.grafo.obtener_vertices()





# -------- WRAPPERS PRIMITIVAS ---------

from grafo import Grafo


def grafo_crear(nombre_archivo):
    """
    Crea un grafo de conexiones de actores a partir de un archivo de datos.

    PRE: Recibe el nombre de un archivo separado por comas que contenga de lineas:
        actor,pelicula,pelicula,pelicula
        que equivalen a: vertice,arista,arista,arista
    POST: Devuelve un grafo creado a partir de estos datos.
    """
    return Grados(nombre_archivo)

def camino(grados, origen, llegada):
    """
    Devuelve el camino entre un actor de origen y uno de llegada.

    PRE: Recibe el grados, un actor de origen y un actor de llegada.
    POST: Devuelve una lista ordenada de cadenas (películas) para llegar desde
        el origen hasta el final.
    """

    return grados.camino(origen, llegada)


def actores_a_distancia(grados, origen, n):
    """
    Devuelve los actores a distancia n del recibido.

    PRE: Recibe el grados, el actor de origen y el número deseado.
    POST: Devuelve la lista de cadenas (actores) a n pasos del recibido.
    """

    return grados.actores_a_distancia(origen, n)


def popularidad(grados, actor):
    """
    Calcula la popularidad del actor recibido.

    PRE: Recibe el grados y un actor de origen
    POST: Devuelve un entero que simboliza la popularidad: todos los adyacentes
        de los adyacentes del actor, multiplicado por su cantidad de peliculas
    """

    return grados.popularidad(actor)


def similares(grados,origen, n):
    """
    Calcula los n actores más similares al actor de origen y los devuelve en una
    lista ordenada de mayor similitud a menor.

    PRE: Recibe el grados, el actor de origen, y el n deseado
    POST: Devuelve una lista de los n actores no adyacentes más similares al
        pedido. La lista no debe contener al actor de origen.
    """
    return grados.similares(origen, n)

