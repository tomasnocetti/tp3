class Arista:
  def __init__(self, vertices, conexion, peso):
    self.vertices = vertices
    self.conexion = conexion
    self.peso = peso

  def __str__(self):
    return str(self.vertices)

  def __str__(self):
    for actor in self.vertices:
      for pelicula in self.vertices[actor]:
        print(actor, self.vertices[actor][pelicula].conexion, self.vertices[actor][pelicula])
    return ""

class Grafo:

  def __init__(self):
    self.vertices = {}
    self.aristas = {}

  def pertenece(self, vertice):
    return vertice in self.vertices

  def agregar_vertice(self, clave):
    if self.pertenece(clave): return
    self.vertices[clave] = set()

  def cantidad_de_vertices(self):
    return (len(self.vertices))

  def agregar_arista_multiple(self, vertices, conexion, peso = 0):
    # Validamos que todo elemento en datos sea un vertice.
    for vertice in vertices:
      if not self.pertenece(vertice): return

    self.aristas[conexion] = self.aristas.get(conexion, Arista(vertices, conexion, peso))

    for vertice in vertices:
      if vertice not in self.aristas[conexion].vertices:
        self.aristas[conexion].vertices.append(vertice)
      self.vertices[vertice].add(conexion)

  def agregar_arista(self, vertice1, vertice2, conexion, peso = 0):
    self.agregar_arista_multiple([vertice1, vertice2], conexion, peso)

  def borrar_vertice(self, vertice):
    if not vertice in self.vertices: return

    aristas = self.vertices[vertice]
    for arista in aristas:
      aristas[arista].discard(vertice)

    self.vertices.pop(vertice)

  def obtener_vertices(self):
    vertices = self.vertices.keys()
    return vertices

  def obtener_adyacentes(self, vertice):
    res = set()
    for arista in self.vertices[vertice]:
      res.update(self.aristas[arista].vertices)
    return res

  def obtener_arista(self, vertice1, vertice2):
    if not self.pertenece(vertice1) or not self.pertenece(vertice2): return None

    for arista in self.vertices[vertice1]:
      if vertice2 in self.aristas[arista].vertices:
        return arista

  def obtener_aristas(self, vertice):
    if not self.pertenece(vertice): return []
    return list(self.vertices[vertice])
