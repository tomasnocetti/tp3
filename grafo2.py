class Arista:
  def __init__(self, vertices, conexion, peso):
    self.vertices = vertices
    self.conexion = conexion
    self.peso = peso

  def __str__(self):
    return str(self.vertices)

class Grafo:

  def __init__(self):
    self.vertices = {}

  def agregar_vertice(self, clave):
    if clave in self.vertices: return
    self.vertices[clave] = {}
    # print(self.vertices)

  def agregar_arista(self, datos, conexion, peso = 0):
    # Validamos que todo elemento en datos sea un vertice.
    for dato in datos:
      if dato not in self.vertices: return

    arista = Arista(datos, conexion, peso)
    for dato in datos:
      self.vertices[dato][conexion] = arista

  def borrar_vertice(self, vertice):
    if not vertice in self.vertices: return

    aristas = self.vertices[vertice]
    for arista in aristas:
      aristas[arista].discard(vertice)

    self.vertices.pop(vertice)

  # def borrar_arista(self, datos):
  #   for vertice in datos:
  #     if vertice not in self.vertices: return

  #   for vertice in datos:
  #     for arista in self.vertices[vertice]:
         

  #   self.arista.pop(arista)


  # def obtener_adyacentes(self, vertice):
  #   adyacentes = []
  #   if (vertice not in self.vertices) return adyacentes

  #   for arista in self.vertices[vertice]:
  #     for adyacente in self.aristas[arista]:


  #   return adyacentes

  def obtener_vertices(self):
    vertices = self.diccionario.keys()
    return vertices

  # def obtener_aristas(self):
  #   aristas = []
  #   for vertice in self.diccionario:
  #     for arista in self.diccionario[vertice]:
  #       for info in self.diccionario[vertice][arista].info:
  #         if (info not in aristas):
  #           aristas.append(info)
  #   return aristas

  def obtener_adyacentes(self, vertice):

    res = set()
    print("ACA")
    # for arista in self.vertices[vertice]:
      # res.union()

    return res 

  def __str__(self):
    for actor in self.vertices:
      for pelicula in self.vertices[actor]:
        print(actor, self.vertices[actor][pelicula].conexion, self.vertices[actor][pelicula])
    return ""

  # def cantidad_de_vertices(self):
  #   return (len(self.diccionario))

  # def obtener_arista(self,vertice1,vertice2):
  #   if (vertice1 in self.diccionario and vertice2 in self.diccionario):
  #     return self.diccionario[vertice1][vertice2].info
