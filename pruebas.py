from grafo import Grafo
import kevin_bacon as kevin
import seis_grados as lib



def pruebas_grafo():
  grafo = Grafo()
  grafo.agregar_vertice("Robin Williams")
  grafo.agregar_vertice("Matt Damond")
  grafo.agregar_vertice("Brad Pitt")
  grafo.agregar_vertice("Adam Sandler")
  grafo.agregar_arista("Robin Williams","Matt Damond","En busca de la felicidad")
  grafo.agregar_arista("Robin Williams","Brad Pitt","Marte")
  grafo.agregar_arista("Robin Williams","Adam Sandler","Azul")
  grafo.agregar_vertice("Guillermo Franchela")
  grafo.agregar_vertice("Ricardo Darin")
  grafo.agregar_arista("Guillermo Franchela","Ricardo Darin","El secreto de sus ojos")
  if(grafo.agregar_arista("PERRo","Ricardo Darin","El secreto de sus ojos") == False):
    print("No se agrego")
  lista = grafo.obtener_vertices()
  print (lista)
  lista2 = grafo.obtener_aristas()
  print(lista2)
  print(str(grafo.cantidad_de_vertices()) + " Vertices")
  grafo.borrar_arista("Guillermo Franchela","Ricardo Darin")
  grafo.borrar_arista("Robin Williams","Matt Damond")

  # similares = lib.similares(grafo,"Robin Williams", 2)
  # similares
  # print(similares)

  print (grafo)

def pruebas_seis_grados():

  grados = lib.Grados("actores.csv")

  padres,costos = grados._camino_minimo("Bacon Kevin")
  print ("__________CAMINO MINIMO__________")
  print (str(padres) + '\n\n\n' + str(costos) + '\n')

  # print ("__________BFS__________")
  # padres,costos,visitados = grados.BFS("Leonardo Dicaprio")
  # print (str(padres) + '\n' + str(costos) + '\n' + str(visitados) + '\n')

  camino = grados.camino("Eastwood Clint","Bale Christian")
  print("CAMINO")
  print (camino)


  actores = grados.actores_a_distancia("Eastwood Clint", 2)
  print("\nActores a distancia 2")
  print (actores)

  popularidad = grados.popularidad("Eastwood Clint")
  print (popularidad)

  similares = grados.similares("Bacon Kevin", 4)
  print(similares)

def pruebas_kevin_bacon():
  
  grados = lib.Grados("actores.csv")

  lib.camino_hasta_KB(grados,"Watts Naomi") 
  lib.bacon_number(grados,"Watts Naomi")
  lib.bacon_number_mayor_a_6(grados)
# 
def main():
  # pruebas_grafo()
  # pruebas_seis_grados()
  pruebas_kevin_bacon()

main()



