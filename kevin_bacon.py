#!/usr/bin/python3
# coding=utf-8
print("\n")
import sys, cmd, helpers, seis_grados as lib
from collections import Counter

KB = "Bacon Kevin"
WORDING = {
  "camino_hasta_KB": {
    "res" : "{} actuó con {} en {}.",
    "err" : "No existe el actor ingresado",
    "sin" : "No hay conexion entre Kevin Bacon y {}"
  },
  "bacon_number": {
    "res": "{} tiene un Kevin Bacon Number igual a {}.",
    "err": "{} no posee un Kevin Bacon Number"
  },
  "bacon_number_mayor_a_6": {
    "intro": "Los actores con un KBN mayor a 6 son:",
    "res": "Con KBN igual a {}: {} actores"
  },
  "KBN_promedio": "El Kevin Bacon Number promedio es {}",
  "similares_a_KB": "Los {} actores más similares KB son {}",
  "bacon_number_infinito": "Los actores con un Bacon Number infinito son {}",
  "popularidad_contra_KB": "{} es un {} de lo popular que es Kevin Bacon",
  "cantidad": "El dataset contiene {} {}."
}



grados = None

class SCCShell(cmd.Cmd):

  def do_camino_hasta_KB(self, params):
    """ 
    Imprime el camino más corto con el cual se llega desde cualquier actor hasta Kevin Bacon. 
    De no haber camino posible se debe imprimir un mensaje acorde (por ejemplo: “No hay conexion entre KB y el actor”), 
    y de no existir el actor ingresado se debe imprimir un mensaje acorde.
    
    """
    try:

      camino1 = grados.camino(params, KB)

      if camino1 == None:
        print(WORDING["camino_hasta_KB"]["err"])
      elif camino1 == []:
        print (WORDING["camino_hasta_KB"]["sin"].format(params))
      else:
        for conexion in camino1:
          print(WORDING["camino_hasta_KB"]["res"].format(conexion[0], conexion[1], conexion[2]))

    except:
      pass

  def do_bacon_number(self, params):
    """
    Imprime el Kevin Bacon Number del actor recibido. Para representar un KBN infinito (no hay conexión entre KB y el actor) 
    el KBN esperado es -1, y de no existir el actor ingresado se debe imprimir un mensaje acorde. 
    Tener en cuenta que el KBN de Kevin Bacon es 0.
    
    """
    try:

      if len(params)  < 1: return
      
      camino1 = grados.camino(params, KB)

      if (camino1 != None):
        number = len(camino1)
        if number == 0 and params != KB: number -= 1
        print(WORDING["bacon_number"]["res"].format(params, number))
      else:
        print(WORDING["bacon_number"]["err"].format(params))
          
    except:
      pass
      
  def do_bacon_number_mayor_a_6(self, params):
    """
    Imprime la cantidad de actores a una distancia mayor a 6 pasos de Kevin Bacon. 
    De no existir actores a más pasos que 6, se imprime un mensaje acorde. 
    En este numero no influyen la cantidad de actores con un KBN infinito 
    
    """
    padres, costos = grados.camino_minimo(KB)
  
    counter = Counter()
    
    for vertices in costos:
      if costos[vertices] >= 6:
        counter[costos[vertices]] += 1
    
    elementos = list(counter.most_common(len(counter)))
    elementos.sort()

    print(WORDING["bacon_number_mayor_a_6"]["intro"])
    for distancia in elementos:
      print(WORDING["bacon_number_mayor_a_6"]["res"].format(distancia[0], distancia[1]))

  def do_bacon_number_infinito(self, params):
    """
    Imprime la cantidad de actores con un KBN infinito. 
    De no haber, se debe imprimir un mensaje acorde.
    
    """
    padres, costos = grados.camino_minimo(KB)
  
    sumatoria = 0
    for actor in grados.obtener_actores():
      if actor in costos: continue
      sumatoria += 1

    print(WORDING["bacon_number_infinito"].format(sumatoria))

  def do_KBN_promedio(self, params):
    """
    Imprime el Kevin Bacon Number promedio. En este numero no influyen la cantidad de actores 
    con un KBN infinito, pero si lo hace el KBN de Kevin Bacon.
    
    """

    padres, costos = grados.camino_minimo(KB)

    counter = Counter()

    promedio = 0  
    
    for costo in costos:
      promedio += costos[costo]

    promedio = promedio / len(costos) 

    print(WORDING["KBN_promedio"].format(round(promedio, 2)))

  def do_similares_a_KB(self, params):
    """
    Imprime una lista de los n actores más similares a Kevin Bacon, ordenados de mayor similitud a menor.

    """
     
    if len(params) < 1: return
    n = helpers.validar_positivo(params)

    similares = list(grados.similares(KB, n))  

    print(WORDING["similares_a_KB"].format(n, similares))


  def do_popularidad_contra_KB(self, params):
    """
    Todo el mundo sabe que Kevin Bacon es el actor más popular de todos. Usando su popularidad como base, 
    imprime en porcentaje cuán popular es el actor en comparación a KB. 
    De no existir el actor ingresado, se debe imprimir un mensaje acorde. 
    Tener en cuenta que Kevin Bacon es un 100% de lo popular que es Kevin Bacon
    
    """
    if len(params) < 1: return 
    
    if params not in grados.obtener_actores():
      print(WORDING["camino_hasta_KB"]["err"])
      return None

    popu_KB = grados.popularidad(KB)

    popu_actor = grados.popularidad(params)

    popu =  round((popu_actor/popu_KB) * 100, 2)


    print(WORDING["popularidad_contra_KB"].format(params, str(popu) + "%"))

  def do_cantidad_peliculas(self, params):
    """ 
    
    Imprime la cantidad de películas en el dataset.

    """
    print(WORDING["cantidad"].format(grados.cantidad_peliculas(), "peliculas"))

  def do_cantidad_actores(self, params):
    """ 
    imprime la cantidad de actores en el dataset.

    """
    print(WORDING["cantidad"].format(grados.cantidad_actores(), "actores"))

  def do_exit(self, params):
    return True

archivo = sys.argv[1]
if __name__ == '__main__' and archivo and helpers.obtener_directorio(archivo):
  grados = lib.Grados(archivo)
  SCCShell().cmdloop()


